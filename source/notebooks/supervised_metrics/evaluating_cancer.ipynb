{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "# Evaluation metrics on classification of breast cancer dataset\n",
    "\n",
    "The confusion matrix is a table that contains the performance of the model: The columns represent the instances that belong to a predicted class; and the rows refer to the instances that actually belong to that class (ground truth).\n",
    "\n",
    "| Confusion matrix | Predicted: True | Predicted: False |\n",
    "|------------------|-----------------|------------------|\n",
    "| Actual: True     | True Positives  | False Negatives  |\n",
    "| Actual: False    | False Positives | True Negatives   |\n",
    "\n",
    "Accuracy measures the model's ability to correctly classify all instances, and not always a useful metric when the objective is to minimize/maximize the occurrence of one class independently of its performance on other classes.\n",
    "\n",
    "Precision measures the model's ability to correctly classify positive labels by comparing it with the total number of instances predicted as positive. This is represented by the ratio between the true positives and the sum of the true positives and false positives.\n",
    "\n",
    "Recall measures the number of correctly predicted positive labels against all positive labels. This is represented by the ratio between true positives and the sum of true positives and false negatives."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "<a id=\"importing\"></a>\n",
    "## Importing libraries and packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Mathematical operations and data manipulation\n",
    "import pandas as pd\n",
    "\n",
    "# Dataset\n",
    "from sklearn.datasets import load_breast_cancer\n",
    "\n",
    "# Model\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn import tree\n",
    "from sklearn.metrics import confusion_matrix\n",
    "from sklearn.metrics import accuracy_score\n",
    "from sklearn.metrics import precision_score\n",
    "from sklearn.metrics import recall_score\n",
    "\n",
    "# Warnings\n",
    "import warnings\n",
    "\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "<a id=\"set-paths\"></a>\n",
    "## Set paths"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "# Path to datasets directory\n",
    "data_path = \"./datasets\"\n",
    "# Path to assets directory (for saving results to)\n",
    "assets_path = \"./assets\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "<a id=\"loading-data\"></a>\n",
    "## Loading dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "[sklearn.datasets.load_breast_cancer](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_breast_cancer.html) - The output is a dictionary-like object, which separates the features (callable as data) from the target (callable as target) into two attributes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "dataset = load_breast_cancer()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "<a id=\"training\"></a>\n",
    "## Partitioning and training"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Shape of X:  (569, 30)\n",
      "Shape of Y:  (569, 1)\n"
     ]
    }
   ],
   "source": [
    "# Convert each attribute (data and target) into a Pandas DataFrame\n",
    "X = pd.DataFrame(dataset.data)\n",
    "Y = pd.DataFrame(dataset.target)\n",
    "\n",
    "print(\"Shape of X: \", X.shape)\n",
    "print(\"Shape of Y: \", Y.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Shape of X_train:  (512, 30)\n",
      "Shape of X_test:  (57, 30)\n",
      "Shape of Y_train:  (569, 1)\n",
      "Shape of Y_test:  (57, 1)\n"
     ]
    }
   ],
   "source": [
    "# First split of the data using the train_test_split function\n",
    "X_train, X_test, Y_train, Y_test = train_test_split(\n",
    "    X, Y, test_size=0.1, random_state=0\n",
    ")\n",
    "\n",
    "print(\"Shape of X_train: \", X_train.shape)\n",
    "print(\"Shape of X_test: \", X_test.shape)\n",
    "print(\"Shape of Y_train: \", Y.shape)\n",
    "print(\"Shape of Y_test: \", Y_test.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "model = tree.DecisionTreeClassifier(random_state=0)\n",
    "model = model.fit(X_train, Y_train)\n",
    "Y_pred = model.predict(X_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "<a id=\"metrics\"></a>\n",
    "## Metrics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "### Confusion matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[21,  1],\n",
       "       [ 6, 29]])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "confusion_matrix(Y_test, Y_pred)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "### Accuracy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "accuracy: 0.8771929824561403\n"
     ]
    }
   ],
   "source": [
    "accuracy = accuracy_score(Y_test, Y_pred)\n",
    "print(\"accuracy:\", accuracy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "### Precision"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "precision: 0.9666666666666667\n"
     ]
    }
   ],
   "source": [
    "precision = precision_score(Y_test, Y_pred)\n",
    "print(\"precision:\", precision)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "### Recall"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "recall: 0.8285714285714286\n"
     ]
    }
   ],
   "source": [
    "recall = recall_score(Y_test, Y_pred)\n",
    "print(\"recall:\", recall)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "ml",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.15"
  },
  "vscode": {
   "interpreter": {
    "hash": "dc605cb69470c71e260a82c6f53f4c82ec34d4e9adf04e6a48fdc4a17829f635"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
