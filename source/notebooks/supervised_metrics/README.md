# Supervised learning metrics

Machine learning is about building a predictive model using historical data to make predictions on new data. The output is probabilistic and evaluating the predictions is crucial.

These notebooks serve us to practice some ML techniques, and as snippets to build on.

- [Partitioning the wine dataset](partitioning_wine.ipynb)
- [Partitioning the handwritten digits dataset](partitioning_digits.ipynb)
- [Evaluation metrics on classification of breast cancer dataset](evaluating_cancer.ipynb)
- [Evaluation metrics on regression of boston dataset](evaluating_boston.ipynb)
- [Evaluating metrics on classification of the digits dataset](evaluating_digits.ipynb)
- [Error analysis on a model trained to recognize handwritten digits](errors_digits.ipynb)
