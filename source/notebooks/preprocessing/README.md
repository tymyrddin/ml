# Preprocessing

Data preprocessing is an integral step in Machine Learning as the quality of data and the useful information that can be derived from it directly affects the ability of our model to learn; And more, reproducability in the entire pipeline to deployment is at stake.

These notebooks serve us to practice some ML techniques, and as snippets to build on.

- [Tips preprocessing](tips.ipynb)
- [Ames Housing EDA](housing-eda.ipynb)
- [Ames Housing feature engineering](housing-feature-engineering.ipynb)
- [Ames Housing feature selection](housing-feature-selection.ipynb)
- [Ames Housing model training](housing-model-training.ipynb)
- [Ames Housing scoring new data](housing-scoring-new-data.ipynb)
- [Ames Housing feature engineering with open source](housing-open-source.ipynb)
- [Titanic preprocessing](titanic.ipynb)
- [Titanic predictions](predicting-survival-titanic.ipynb)
